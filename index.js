//1.Прототипне наслідування дозволяє одному об'єкту базуватися на іншому, батьківському об'єкту. Дочірній об'єкт бере властивості від
//батьківського за замовчуванням, якщо вони в самому дочірньому об'єкті не змінюються.
//2.Super() викликає батьківський конструктор. Дає доступ до властивостей та методів батьківського конструктора.


class Employee {
    _name;
    _age;
    _salary;

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age;
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    _lang;

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    get lang() {
        return this._lang;
    }

    set lang(lang) {
        this._lang = lang;
    }
}

let employee = new Employee('Jack', '23', '5000');
let programmer = new Programmer('Nick', '25', '6000', 'js');
let programmer2 = new Programmer('Kate', '22', '7000', 'php');
let programmer3 = new Programmer('Julia', '28', '9000', 'c#');


console.log('Create employee', employee);
console.log('create programmer', programmer);
console.log('create programmer2', programmer2);
console.log('create programmer3', programmer3);


console.log('Nick`s(programmer`s) salary:', programmer.salary);
console.log('Kate`s(programmer2`s) salary:', programmer2.salary);
console.log('Julia`s(programmer3`s) salary:', programmer3.salary);

console.log(programmer.name);